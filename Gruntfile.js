// Gruntfile.js
// наша функция-обёртка (требуется для Grunt и его плагинов)
// все настройки располагаются внутри этой функции
module.exports = function(grunt) {

    // ===========================================================================
    // НАСТРОЙКА GRUNT ===========================================================
    // ===========================================================================
    grunt.initConfig({
        // получить конфигурацию из package.json
        // так мы можем использовать штуки вроде name и version (pkg.name)
        pkg: grunt.file.readJSON('package.json'),

        // вся наша конфигурация будет здесь

        uglify: {
            options: {
                banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
            },
            prod: {
                files:{
                    'app/static/js/scripts.min.js':'app/src/js/scripts.js'
                },
                options:{
                    report:'min',
                    mangle:true,
                    beautify: true
                }
            },

            init: {
                files:{
                    'app/static/js/lib/util.min.js':['app/src/js/util.js'],
                    'app/static/js/lib/main.min.js':['app/src/js/main.js']
                },
                options:{
                    report:'min',
                    mangle:true
                }
            },

            dev :{
                files: {
                    'app/static/js/scripts.js':'app/src/js/scripts.js'
                },
                options:{
                    beautify:true,
                    report:'min',
                    sourceMap:true
                }
            }
        },

        jshint: {
            options: {
                reporter: require('jshint-stylish')
            },
            src: [
                'Gruntfile.js', 'app/src/js/scripts.js'
            ]
        },

        watch :{
            src:{
                files:['Gruntfile.js', 'app/src/js/*.js','app/src/sass/*','app/templates/*'],
                tasks:['jshint','uglify:dev','sass:dev'],
                options:{
                    atBegin:true
                }
            }
        },

        concat: {
            options: {
                //banner: '<%= banner %>\n<%= jqueryCheck %>\n<%= jqueryVersionCheck %>',
                //stripBanners: false
            },

            src: {
                src: ['app/src/js/*.js'],
                dest: 'app/assets/_concat_scripts.js'
            }

        },

        sass: {
            init:{
                options: {
                    style:'compressed'
                },
                files:{
                    'app/static/css/ie8.min.css':'app/src/sass/ie8.scss',
                    'app/static/css/ie9.min.css':'app/src/sass/ie9.scss'
                }
            },
            dev: {
                options: {
                    style:'expanded',
                           },
                files: {
                    'app/static/css/main.css':'app/src/sass/top.scss'
                }
            },
            prod: {
                options: {
                    style:'compressed'

                },
                files:{
                    'app/static/css/main.min.css':'app/src/sass/top.scss'
                }
            }
        },

        copy:{

            fonts:{
                expand:true,
                cwd:'app/src/fonts/',
                src:['**'],
                dest: 'app/static/fonts/'
            },

            jquery: {
                expand:true,
                cwd: 'bower_components/jquery/dist/',
                src: ['jquery.min.js'],
                dest: 'app/static/js/lib/'
            },
            skel:{
                expand:true,
                cwd: 'bower_components/skel/dist/',
                src: ['skel.min.js'],
                dest:'app/static/js/lib/'
            },
            respond:{
                expand:true,
                cwd: 'bower_components/respond/dest/',
                src:['respond.min.js'],
                dest:'app/static/js/lib/ie/'
            },
            html5shiv:{
                expand:true,
                cwd: 'bower_components/html5shiv/dist/',
                src:['html5shiv.js'],
                dest:'app/static/js/lib/ie/'
            },
            font_awesome:{
                expand:true,
                cwd:'app/src/css/',
                src:['font-awesome.min.css'],
                dest:'app/static/css/'
            }

        }
    });

    // ===========================================================================
    // ЗАГРУЗКА ПЛАГИНОВ GRUNT ===================================================
    // ===========================================================================
    // мы можем их загрузить, только если они находятся в нашем package.json
    // убедитесь, что вы запустили npm install, чтобы наше приложение могло их найти
    grunt.registerTask('default',['jshint', 'copy','uglify','sass']);
    grunt.registerTask('init',['copy','concat','jshint','uglify', 'sass']);
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    //grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks("grunt-contrib-concat");
    //grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-copy');

};
