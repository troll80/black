#-*- coding:utf-8 -*-

from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView

from app.main.models import User, Event, Location, Image, Tags, Mail, About, Sidebar, PageContent
from app.authorize.views import AdminIndexView
from app.authorize.models import AdminUser

from app.admin_ext import *

import os.path as op

path = op.join(op.dirname(__file__), 'app','static')

def create_admin(app, db):
    """
    Добавляется модели для отображения в админке.
       """
    admin = Admin(app, name= 'black',index_view = AdminIndexView(), template_mode = 'bootstrap3')

    admin.add_view(UserView(User, db.session))
    admin.add_view(EventView(Event, db.session))
    #admin.add_view(ModelView(AdminUser, db.session))
    admin.add_view(GeoModelView(Location, db.session))
    #admin.add_view(ImageView(Image, db.session))
    admin.add_view(FileAdmin(path,'/static/',name= 'Static Files'))
    admin.add_view(TagsView(Tags, db.session))
    admin.add_view(MailView(Mail, db.session))
    admin.add_view(AboutView(About, db.session))
    admin.add_view(SidebarView(Sidebar, db.session))
    admin.add_view(PageView(PageContent, db.session))
