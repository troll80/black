#-*- coding:utf-8 -*-
import os
import os.path as op
import shortuuid
from datetime import date
from werkzeug.utils import secure_filename
from flask import request
from flask import url_for
from flask.ext.admin.contrib import sqla
from flask.ext.admin import helpers, expose
from flask.ext.admin.contrib.geoa import ModelView as GeoModelView
from flask.ext import admin, login
from wtforms.widgets import TextArea, CheckboxInput, Select, html_params, HTMLString
from wtforms import validators
from wtforms import TextAreaField, BooleanField
from wtforms import fields
from jinja2 import Markup
from flask_admin import form as form_admin
from flask_admin.contrib.fileadmin import FileAdmin
from flask_admin.model.form import InlineFormAdmin
from flask_admin.contrib.sqla.form import InlineModelConverter
from flask_admin.contrib.sqla.fields import InlineModelFormList
from flask_admin.form import rules
from flask_admin.form import RenderTemplateWidget
from flask_admin.model import typefmt

from app.extlog import logger
from app.database import db
from app.main.models import Image, Tags

file_path = op.join(op.dirname(__file__), 'static', 'images')
#
## TODO занести в конфиг
#CKEDITOR_URL = "https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"
CKEDITOR_URL = 'http://js.nicedit.com/nicEdit-latest.js'

def date_format(view, value):
    return value.strftime('%d.%m.%Y  %H:%M')

MY_DEFAULT_FORMATTERS = dict(typefmt.BASE_FORMATTERS)
MY_DEFAULT_FORMATTERS.update({
    type(None): typefmt.null_formatter,
    date: date_format
})


class BoolChoiceVisible(CheckboxInput):
    """ Переопределие чекбокса для отображения булевого параметра
    """

    def __call__(self, field, **kwargs):
        kwargs['class'] += 'checkbox'
        kwargs['style'] = 'width:40px'
        return super(BoolChoiceVisible, self).__call__(field, **kwargs)


class BoolWidget(BooleanField):
    widget = BoolChoiceVisible()


# This widget uses custom template for inline field list
class CustomInlineFieldListWidget(RenderTemplateWidget):

    def __init__(self):
        super(CustomInlineFieldListWidget, self).__init__(
            'admin/field_list.html')


# This InlineModelFormList will use our custom widget and hide row controls
class CustomInlineModelFormList(InlineModelFormList):
    widget = CustomInlineFieldListWidget()

    def display_row_controls(self, field):
        return False


# Create custom InlineModelConverter and tell it to use our InlineModelFormList
class CustomInlineModelConverter(InlineModelConverter):
    inline_field_list_type = CustomInlineModelFormList


# Customized inline form handler
class InlineModelForm(InlineFormAdmin):
    """ Форма для загрузки картинок.

    """
    form_excluded_columns = ('path',)

    form_label = 'Image'
    # Признак заглавного изображения
    form_overrides = dict(
        is_top=BoolWidget
    )

    def __init__(self):
        return super(InlineModelForm, self).__init__(Image)

    def postprocess_form(self, form_class):
        form_class.upload = fields.FileField('Image')
        return form_class

    def on_model_change(self, form, model):
        """ Загрузка изображение, генерация уникального имени.
        """

        file_data = request.files.get(form.upload.name)

        if file_data:
            uuid_name = '{}.{}'.format(
                shortuuid.uuid()[:6].lower(), file_data.filename.split('.')[1])
            try:
                os.rename(op.join(file_path, file_data.filename),
                          op.join(file_path, uuid_name))
            except FileNotFoundError as e:
                logger.error(e)

            file_data.filename = uuid_name
            model.path = secure_filename(file_data.filename)
            file_data.save(op.join(file_path, model.path))


class CKTextAreaWidget(TextArea):
    """Виджет для текста и разметки.
    """

    def __call__(self, field, **kwargs):
        if kwargs.get('class'):
            kwargs['class'] += ' ckeditor'

        else:
            kwargs.setdefault('class', 'ckeditor')

        return super(CKTextAreaWidget, self).__call__(field, **kwargs)


class CKTextAreaField(TextAreaField):
    widget = CKTextAreaWidget()


class EventView(sqla.ModelView):
    """ Виджет для мероприятия.
     """
    #column_exclude_list = ('sub_title','description', )

    def is_accessible(self):
        return login.current_user.is_authenticated

    # Inline form for Image
    # Инлайн формы для загрузки картинок
    inline_model_form_converter = CustomInlineModelConverter
    inline_models = (InlineModelForm(),)

    #form_choices = {'is_visible': [(1,u'show'),( 0,u'hide')]}

    can_view_details = True
    # Custom data format
    column_type_formatters = MY_DEFAULT_FORMATTERS
    # Widget TextArea,
    extra_js = [CKEDITOR_URL]
    form_excluded_columns = ('tstamp', 'tstamp_last', 'users')
    column_list = ('is_visible', 'is_top', 'title', 'tstamp', 'tstamp_last')

    form_overrides = dict(
        description=CKTextAreaField,
        is_visible=BoolWidget,
        is_top=BoolWidget,
        preview=CKTextAreaField
    )
    # Validate model before save

    def on_model_change(self, form, model, is_created):
        if form.tstamp_end.data and form.tstamp_start.data and(form.tstamp_end.data <= form.tstamp_start.data):
            raise validators.ValidationError(
                'Invalid datatime start and begin')
        else:
            super().on_model_change(form, model, is_created)


class GeoModelView(GeoModelView):
    """Виджет  для карты
    """

    def is_accessible(self):
        return login.current_user.is_authenticated


class TagsView(sqla.ModelView):
    """ Тэги
    """

    def is_accessible(self):
        return login.current_user.is_authenticated
    # Sort

    def get_query(self):
        return self.session.query(self.model).order_by(self.model.id.desc())


class UserView(sqla.ModelView):
    """ Отображение зарегестрированных пользоватлей и их мероприятий.
    """
    column_list = ('name', 'phone', 'email', 'event', 'tstamp')
    can_view_details = True
    column_type_formatters = MY_DEFAULT_FORMATTERS
    form_create_rules = ('event', rules.Text('Foobar'),)

    def is_accessible(self):
        return login.current_user.is_authenticated

# File manager


class FileAdmin(FileAdmin):
    """Файловый менеджер
    """

    def is_accessible(self):
        return login.current_user.is_authenticated


class MailView(sqla.ModelView):
    #column_exclude_list = ('sub_title','description', )

    def is_accessible(self):
        return login.current_user.is_authenticated

    can_view_details = True
    # Custom data format
    column_type_formatters = MY_DEFAULT_FORMATTERS
    # Widget TextArea,
    extra_js = [CKEDITOR_URL]

    form_overrides = dict(
        body=CKTextAreaField,
    )
    # Validate model before save


class AboutView(sqla.ModelView):
    """ для отображение на главной About
    """

    def is_accessible(self):
        return login.current_user.is_authenticated

    def _list_thumbnail(view, context, model, name):
        if not model.image:
            return ''

        return Markup(
            '<img src="{}">'.format(url_for(
                'static', filename=form_admin.thumbgen_filename('images/' + model.image)))
        )

    column_formatters = {
        'image': _list_thumbnail
    }

    form_extra_fields = {
        'image': form_admin.ImageUploadField(
            'Image', base_path=file_path, thumbnail_size=(100, 50, True)
        )
    }


class SidebarView(sqla.ModelView):
    def is_accessible(self):
        return login.current_user.is_authenticated

    extra_js = [CKEDITOR_URL]
    form_overrides = dict(content=CKTextAreaField)


class PageView(sqla.ModelView):
    """ Admin views for page content
   """
    def is_accessible(self):
        return login.current_user.is_authenticated

    extra_js = [CKEDITOR_URL]
    form_overrides = dict(text=CKTextAreaField)
