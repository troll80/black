#-*- coding:utf-8 -*-
from werkzeug.security import generate_password_hash, \
     check_password_hash
     
from flask.ext.login import UserMixin
from app.database import db



class AdminUser(db.Model, UserMixin):
     __tablename__  = 'admin'

     id = db.Column(db.Integer, primary_key = True)
     login = db.Column(db.String(128),nullable = False)
     password_hash = db.Column(db.String(256),nullable = False)

     x = property()

     @x.setter
     def password(self, password):
         self.password_hash = generate_password_hash(password)

     def check_password(self, password):
         return check_password_hash(self.password_hash, password)
