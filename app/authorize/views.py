#-*- coding:utf-8 -*-
import os
import os.path as op
from flask import Blueprint, redirect, url_for, request
from wtforms.widgets import TextArea
from flask.ext import admin, login
from flask.ext.admin import helpers, expose
from flask_admin import form as form_admin
from flask_admin.contrib.fileadmin import FileAdmin
from wtforms import form, fields, validators

from .models import AdminUser
from app.extlog import logger
from app.database import db


file_path = op.join(op.dirname(__file__), '../static', 'images')


def init_login(app):
    login_manager = login.LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = 'login'

    @login_manager.user_loader
    def load_user(user_id):
        admin = db.session.query(AdminUser).get(user_id)
        return admin


class LoginForm(form.Form):
    login = fields.TextField(validators=[validators.required()])
    password = fields.PasswordField(validators=[validators.required()])


    def validate_login(self, field):
        admin = self.get_user()

        if admin is None:
            raise validators.ValidationError('Invalid user')

        if not admin.check_password(self.password.data):
            raise validators.ValidationError('Invalid password')

    def get_user(self):
         return db.session.query(AdminUser).filter(AdminUser.login == self.login.data).first()


class AdminIndexView(admin.AdminIndexView):

    @expose('/')
    def index(self):
        if not login.current_user.is_authenticated:
            return redirect(url_for('.login_view'))

        logger.debug('is authenticated')
        return super(AdminIndexView, self).index()

    @expose('/login/', methods=['POST', 'GET'])
    def login_view(self):

        form = LoginForm(request.form)

        if helpers.validate_form_on_submit(form):
            user = form.get_user()
            login.login_user(user)

        if login.current_user.is_authenticated:
            logger.debug('login.current_user_is_authenticated')
            return redirect(url_for('.index'))

        link = '<p>Don\'t have an account?</p>'
        self._template_args['form'] = form
        self._template_args['link'] = link
        return super(AdminIndexView, self).index()

    @expose('/logout')
    def logout_view(self):
        login.logout_user()
        return redirect(url_for('.index'))
