#-*- coding:utf-8 -*-
import os
import os.path as op
from datetime import datetime
from geoalchemy2 import Geometry
from sqlalchemy.event import listens_for
from sqlalchemy_utils import EmailType, PhoneNumberType
from slugify import slugify
from app.database import db
from flask_admin import form
from app.extlog import logger

month = (u'Января', u'Февраля', u'Марта', u'Апреля', u'Мая', u'Июня',
         u'Июля', u'Августа', u'Сентября', u'Октября', u'Ноября', u'Декабря')


file_path = op.join(op.dirname(__file__), 'images')


class MixinModel:

    def save(self):
        db.session.add(self)
        db.session.commit()

location_table = db.Table('location_table',
                          db.Column('event_id', db.Integer,
                                    db.ForeignKey('event.id')),
                          db.Column('location_id', db.Integer,
                                    db.ForeignKey('location.id'))
                          )


event_tags = db.Table('event_tags',
                      db.Column('tags_id', db.Integer,
                                db.ForeignKey('tags.id')),
                      db.Column('event_id', db.Integer,
                                db.ForeignKey('event.id')))


class Tags(db.Model, MixinModel):
    __tablename__ = 'tags'

    id = db.Column(db.Integer, primary_key=True)
    tagname = db.Column('tagname', db.String(100))

    def __repr__(self):
        return '#{} '.format(self.tagname)


class Location(db.Model, MixinModel):
    __tablename__ = 'location'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column('Name', db.String(80))
    address = db.Column('Address', db.String(256))
    description = db.Column('Describe', db.String(512))

    point = db.Column(Geometry("POINT", srid=4326))

    def __repr__(self):
        return '<{}>'.format(self.name)


class Image(db.Model, MixinModel):

    __tablename__ = 'image'

    id = db.Column(db.Integer, primary_key=True)
    path = db.Column('path', db.Unicode(128))
    is_top = db.Column('is top', db.Boolean, default=False)

    event_id = db.Column(db.Integer, db.ForeignKey('event.id'))

    def __repr__(self):
        return '{}'.format(self.path)


@listens_for(Image, 'before_update')
def update_modified_on_update_listener(mapper, connnection, target):
    if target.is_top == True:
        # reset all is_top
        Image.query.filter_by(is_top=True).filter(Image.id != target.id) \
             .filter(Image.event == target.event).update(dict(is_top=False))


@listens_for(Image, 'after_delete')
def del_image(mapper, contection, target):
    if target.path:
        try:
            os.remove(op.join(file_path, target.path))
        except OSError:
            logger.error('Image {} didn\'t delete'.format(target.path))

        try:
            os.remove(op.join(file_path, form.thumbgen_filename(target.path)))
        except OSError:
            logger.error('Thumbnail {} didn\'t delete'.format(target.path))


class Event(db.Model, MixinModel):

    __tablename__ = 'event'

    id = db.Column(db.Integer, primary_key=True)
    is_visible = db.Column('is_show', db.Boolean, default=False)
    is_top = db.Column('is_top', db.Boolean, default=False)
    is_form = db.Column('is_form',db.Boolean, default =False)
    is_location = db.Column('is_location', db.Boolean, default=False)
    slug = db.Column('slug', db.String(255), index=True)

    title = db.Column('Title', db.String(255), nullable=False)
    sub_title = db.Column('Subtitle', db.String(512))
    preview = db.Column('Preview', db.Text)
    description = db.Column('Description', db.Text)

    tstamp_start = db.Column('Date', db.DateTime)
    tstamp_end = db.Column('Data', db.DateTime)

    tstamp = db.Column(db.DateTime, default=db.func.now())
    tstamp_last = db.Column(db.DateTime)

    location = db.relationship('Location', secondary=location_table,
                               backref=db.backref('event'), lazy='dynamic')

    images = db.relationship(
        'Image', backref=db.backref('event'), lazy='dynamic')

    tags = db.relationship('Tags',
                           secondary=event_tags,
                           backref=db.backref('event'))

    @staticmethod
    def set_top_post(target, value, oldvalue, initiator):
        pass

    @property
    def image_title(self):
        obj = [image for image in self.images if image.is_top]
        if obj:
            return obj[0]
        elif self.images:
            return sorted(self.images, key=id)[0]
        else:
            return None

    @property
    def date(self):
        m = int(self.datetime.strftime('%m'))
        d = self.datetime.strftime('%d')

        return u'{} {}'.format(d, month[m])

    @property
    def time(self):
        return self.datetime.strftime('%H:%M')

    def __repr__(self):
        return u'{}'.format(self.title)


@listens_for(Event, 'before_update')
def update_modified_on_update_listener(mapper, connnection, target):
    # update time
    target.tstamp_last = datetime.utcnow()
    target.slug = slugify(target.title) if not target.slug else target.slug
    if target.is_top == True:
        # reset  is_top
        Event.query.filter_by(is_top=True) \
                   .filter(Event.id != target.id).update(dict(is_top=False))
        target.is_visible = True


class User(db.Model, MixinModel):

    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    phone = db.Column(PhoneNumberType(), nullable=False)
    email = db.Column(EmailType, nullable=False)
    message = db.Column(db.Unicode(1024))
    tstamp = db.Column(db.DateTime, default=datetime.utcnow)

    event = db.relationship('Event', backref=db.backref('users'))
    event_id = db.Column(db.Integer, db.ForeignKey('event.id'))

    def __repr__(self):
        return '<{}>'.format(self.name)


class Mail(db.Model):

    __tablename__  = 'mail'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(128))
    body = db.Column(db.Text)

    event = db.relationship('Event', backref = db.backref('mail'))
    event_id =db.Column(db.Integer, db.ForeignKey('event.id'))


    def __repr__(self):
        return self.title



class About(db.Model):
    """ Отображение кратной информации на главной странице.
       """

    __tablename__ = 'about'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column('title', db.Unicode(128))
    image = db.Column('path', db.Unicode(128))
    text = db.Column('text', db.Unicode(512))


class Sidebar(db.Model):
    """ Контент для сайдбара
     """
    __tablename__ = 'sidebar'

    id = db.Column(db.Integer, primary_key=True)
    content = db.Column('content', db.Unicode(2042))


class PageContent(db.Model):
    """Страницы.
      """
    __tablename__ = 'page'

    id = db.Column(db.Integer, primary_key=True)
    slug = db.Column('URL', db.Unicode(255))
    title = db.Column('Title', db.Unicode(512))
    text = db.Column('Content',db.Unicode(10000))

    def __repr__(self):
        return '<{}>'.format(self.title)
