#!/usr/bin/python
#-*-coding:utf-8-*-

import time
from sqlalchemy.exc import SQLAlchemyError
from shapely import wkb
import phonenumbers
from phonenumbers.phonenumberutil import NumberParseException
from flask import (
    Blueprint,
    abort,
    request,
    url_for,
    render_template,
    redirect,
    jsonify,
    current_app
)
from app.database import db
from app.extlog import logger
from app.main.models import Event, User, About, Sidebar, PageContent
from app.send_mail import send_email

module = Blueprint('task', __name__)

def get_sidebar_content():

    try:
        sidebar = Sidebar.query.first()
    except SQLAlchemyError as e:
        logger.error(e)

    return  getattr(sidebar, 'content', None)


@module.route('/index', methods=['GET'])
@module.route('/', methods= ['GET'])
def index():
    logger.debug(request.args)
    try:
        events = Event.query.filter_by(is_visible = True).order_by(Event.is_top.desc(), Event.tstamp.desc())

    except SQLAlchemyError as e:
        logger.error('Error while querying db {}'.format(e))
        abort(500)

    try:
        about = About.query.all()
    except SQLAlchemyError as e:
        logger.error(e)
        abort(500)

    sidebar_content = get_sidebar_content()
    return render_template('index.html', object_list=events, about=about,
                           sidebar_content=sidebar_content)

@module.route('/ext/page/<slug>', methods=['GET'])
def view_page(slug):
    """ Загрузка страниц создаваемых админом.
     """
    try:
        page = PageContent.query.filter_by(slug = slug).first()
    except SQLAlchemyError as e:
        logger.error('Load page with slug {} {}'.format(slug, e))

    sidebar_content= get_sidebar_content()
    return render_template('page.html', page=page, sidebar_content=sidebar_content)


@module.route('/detail/<slug>', methods=['GET'])
def event(slug):

    try:
        event = Event.query.filter_by(slug = slug).first_or_404()
    except SQLAlchemyError as e:
        logger.error('Error while querying db {}'.format(e))
        abort(500)

    point = event.location

    try:
        location = wkb.loads(bytes(point[0].point.data))
    except IndexError as e:
        #  Широта: 55.7522200° Долгота: 37.6155600°
        location = type('tmp', (object,), dict(y =55.752, x=37.615))
        logger.error('Location point. {}'.format(e))

    sidebar_content= get_sidebar_content()

    return render_template('event.html', object = event,
                           location = {"latitude":location.y , "longitude": location.x},
                           sidebar_content = sidebar_content
    )


def phone_is_validate(data):

     try:
        num = phonenumbers.parse(data['phone'], "RU")
     except NumberParseException as e:
         logger.error('Wrong number {}'.format(e))
         return False

     if not phonenumbers.is_valid_number(num):
         return False

     data['phone'] = phonenumbers.format_number(num, phonenumbers.PhoneNumberFormat.INTERNATIONAL)

     return True


@module.route('/api/registration/', methods=['POST'])
def user_registration():

    #form = RegistrationForm(request.form)
    if request.method == 'POST':
        try:
            slug = request.form.get('url')
        except AttributeError as e:
            logger.error('Error URL in request {}'.format(e))
            abort(500)

        try:
            cur_event = Event.query.filter_by(slug=slug).first()
        except SQLAlchemyError as e:
            logger.error('Error while querying db {}'.format(e))
            flash('Во время запроса произошла непредвиденная ошибка.')
            abort(500)

        try:
            d_data = dict(name = request.form['name'],
                          phone = request.form['tel'],
                          email = request.form['email'],
                          message = request.form['message']
            )
        except KeyError as e:
            logger.error('Error while querying db{}'.format(e))
            abort(500)

        if not phone_is_validate(d_data):
            return jsonify(error =True, msg = 'Phone number is\'t validate')

        try:
            user = User( **d_data)
            user.event = cur_event
            user.save()
        except SQLAlchemyError as e:
            logger.error('There was error while querying db {}'.format(e))
            db.session.rollback()
            return jsonify(error = True, msg = "Error.")
        else:
            ## Емайл оповещение
            send_email(user)

        finally:
            db.session.close()

    return jsonify(error = False, msg = "You are registration!")


@module.errorhandler(404)
def page_not_found(e):
    logger.error('Page not found {}'.format(e))
    return render_template('pages/404.html'), 404
