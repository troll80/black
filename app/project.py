import os
from flask import Flask
from flask_wtf.csrf import CsrfProtect

from app.database import db
from admin import create_admin

csrf = CsrfProtect()


def create_app():
    """ Создание основного приложения.
        иницилизация блюпринтов
    """
    app = Flask(__name__)
    # app.config.from_object('config.ProductionConfig')
    app.config.from_object(os.environ['APP_SETTINGS'])
    db.init_app(app)

    csrf.init_app(app)

    with app.test_request_context():
        db.create_all()

        if app.debug:
            try:
                from flask_debugtoolbar import DebugToolbarExtension
                toolbar = DebugToolbarExtension(app)
            except:
                pass

    import app.main.views as main

    app.register_blueprint(main.module)

    from app.verify import views as validate
    app.register_blueprint(validate.module)

    import app.authorize.views as login
    # csrf.exempt(login.module)

    # app.register_blueprint(login.module)
    login.init_login(app)

    create_admin(app, db)

    return app
