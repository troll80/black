#-*- coding:utf-8 -*-
import time
from threading import Thread
from flask import render_template
from flask_mail import Message
from app.main.models import Mail, User
from app.extlog import logger

# initialze app
app = None
mail = None


def init_mail(iapp, imail):
    global app, mail
    app = iapp
    mail = imail


def _send_async_email(msg):
    with app.app_context():
        mail.send(msg)
        #logger.info('Email:%s' % msg)


def _send_email_to_admin(user):
    emails = app.config.get('ADMINS')

    event = user.event
    msg = Message('Регистрация на {}'.format(event),
                  sender=emails[0],
                  recipients=emails,
                  html='<h4>Зарегистрировался имя: {user.name} \
                телефон: {user.phone} \
                емайл: {user.email} \
                сообщение от пользователя: {user.message} \
                время регистрации: {user.tstamp} \
                мероприятие:{event}</h4>'.format(user=user, event=event))
    thr = Thread(target=_send_async_email, args=[msg])
    thr.start()


def _send_email_to_user(user):
    dbmail = Mail.query.filter_by(event=user.event).one()

    msg = Message(dbmail.title,
                  sender=app.config.get('ADMINS')[0],
                  recipients=[user.email], html=dbmail.body)

    thr = Thread(target=_send_async_email, args=[msg])
    thr.start()


def send_email(user):
    # Посылается письмо пользовтелюи админу
    _send_email_to_user(user)
    time.sleep(1)
    _send_email_to_admin(user)
