  /*
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '428265520908276',
      xfbml      : true,
      version    : 'v2.10'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/ru_RU/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
*/

$('form').submit(function(event){
    var form = $(this);
    //var csrftoken = $('meta[name=csrf-token]').attr('content');
    //var csrf_token = "{{ csrf_token() }}";
    var csrf_token = $('meta[name=csrf-token]').attr('content');

    var data = form.serializeArray();
    //event.preventDefault();
    _url = window.location.pathname.split('/')[2];
    data.push({ name: 'url', value : _url });

    $.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrf_token);
        }
    }
    });


    $.ajax({
	type:'POST',
	url:'/api/registration/',
	dataType : 'json',
	data : data,
	beforeSend: function(xhr, settins){

	    //alert('before');
	    form.find('input[type="submit"]').attr('disabled','disabled');
	    form.find('#notification')
                .removeClass()
                .addClass('_progress')
                .html('Please wait...')
                .show();

	},
	success : function(data){
	    if (data.error){
		console.error(data.error);
		form.find('#notification').addClass('_error').html(data.msg).show();
	    } else {
                console.info('data.msg');
		form.find('#notification').addClass('_success').html(data.msg).show();
		form[0].reset();
	    }
	},
	error : function(xhr, ajaxOption,throwError){
	    console.error(xhr.status);
	    console.error(throwError);
            form.find("#notification").addClass('_error').html('Server fail').show();
	},
	complete : function(data) {
	    //form.find('#notification').removeClass().hide();
	    form.find('input[type="submit"]').prop("disabled",false);
	}

    });

   // event.preventDefault();
    return false;
});
