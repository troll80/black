from flask import (
    Blueprint,
    render_template,
    )

from app.extlog import logger

module = Blueprint('confirm', __name__ )

@module.route('/index/google2eeded8a86ab643a.html', methods=['GET'])
def confirm_google():
	return render_template('google2eeded8a86ab643a.html')
