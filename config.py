#-*- coding:utf-8 -*-

import os

ROOT_PATH = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = False
    CSRF_ENABLED = True
    SECRET_KEY = 'kld?ZpU=nxyu>EC]'

    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLACHEMY_ECHO = False

    MAPBOX_MAP_ID = 'mapbox.outdoors'
    MAPBOX_ACCESS_TOKEN = 'pk.eyJ1Ijoic2h1cmljODAiLCJhIjoiY2oyN2liNTltMDAzMDMzbW54Y3ZmbngyMSJ9.uRASS42dkFbG8ng7qxd1Yg'

    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 465
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'backtoblackproject@gmail.com'
    MAIL_PASSWORD = os.environ['MAIL_PASSWORD']
    MAIL_MAX_EMAILS = 20
    # administrator list
    ADMINS = ['backtoblackproject@gmail.com']
    CKEDITOR_URL = "https://cdn.ckeditor.com/4.9.1/full-all/ckeditor.js"


class DebugConfig(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = True

class ProductionConfig(Config):
    DEBUG = False
    SQLACHEMY_ECHO = False
