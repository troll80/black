#! /usr/bin/env python3
#-*- coding:utf-8 -*-
import os
from flask_script import Manager
#from flask_assets import Bundle, Environment
from flask.ext.migrate import Migrate, MigrateCommand
from flaskext.markdown import Markdown
from jinja2_maps import activate_filters
from flask_mail import Mail
from app.project import create_app
from app.database import db
from app.authorize.models import AdminUser
from app.send_mail import init_mail

app = create_app()
app.config.from_object(os.environ['APP_SETTINGS'])
# app.config.from_object('config.ProductionConfig')

# менеджер приложения
manager = Manager(app)

#приложение миграции
migrate = Migrate(app, db)

#разметка
Markdown(app)
activate_filters(app.jinja_env)

#email
mail = Mail(app)
init_mail(app, mail)

manager.add_command('db', MigrateCommand)


@manager.command
def create_admin():
    """Добавляется администратор
       """
    admin = AdminUser()
    admin.login = 'YliaROmanova'
    admin.password = 'E8nX5?HALVF$*?6N'
    db.session.add(admin)

    try:
        db.session.commit()
    except:
        db.session.rollback()
    finally:
        db.session.close()


if __name__ == '__main__':
    manager.run()
