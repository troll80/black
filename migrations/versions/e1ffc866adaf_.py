"""empty message

Revision ID: e1ffc866adaf
Revises: 6e37d5ad0b44
Create Date: 2017-09-14 21:42:40.918924

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e1ffc866adaf'
down_revision = '6e37d5ad0b44'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    #op.drop_table('spatial_ref_sys')
    op.add_column('event', sa.Column('Main Image', sa.String(length=128), nullable=True))
    op.drop_column('image', 'is_title ')
    op.drop_column('image', 'Name')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('image', sa.Column('Name', sa.VARCHAR(length=64), autoincrement=False, nullable=True))
    op.add_column('image', sa.Column('is_title ', sa.BOOLEAN(), autoincrement=False, nullable=True))
    op.drop_column('event', 'Main Image')
    op.create_table('spatial_ref_sys',
    sa.Column('srid', sa.INTEGER(), autoincrement=False, nullable=False),
    sa.Column('auth_name', sa.VARCHAR(length=256), autoincrement=False, nullable=True),
    sa.Column('auth_srid', sa.INTEGER(), autoincrement=False, nullable=True),
    sa.Column('srtext', sa.VARCHAR(length=2048), autoincrement=False, nullable=True),
    sa.Column('proj4text', sa.VARCHAR(length=2048), autoincrement=False, nullable=True),
    sa.CheckConstraint('(srid > 0) AND (srid <= 998999)', name='spatial_ref_sys_srid_check'),
    sa.PrimaryKeyConstraint('srid', name='spatial_ref_sys_pkey')
    )
    # ### end Alembic commands ###
